# https://github.com/hwalsuklee/tensorflow-mnist-VAE

import matplotlib
matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
from imageio import imwrite
from skimage.transform import resize as imresize
import math
from rs4 import pathtool
import shutil, os

class Plot_Reproduce_Performance():
    def __init__(self, DIR, n_img_x=8, n_img_y=8, img_w=28, img_h=28, resize_factor=1.0):
        self.DIR = DIR
        pathtool.mkdir (DIR)
        assert n_img_x > 0 and n_img_y > 0

        self.n_img_x = n_img_x
        self.n_img_y = n_img_y
        self.n_tot_imgs = n_img_x * n_img_y

        assert img_w > 0 and img_h > 0

        self.img_w = img_w
        self.img_h = img_h

        assert resize_factor > 0

        self.resize_factor = resize_factor

    def reset (self):
        if os.path.isdir (self.DIR):
            shutil.rmtree (self.DIR)
        pathtool.mkdir (self.DIR)

    def add_noise (self, images):
        images = images * np.random.randint (2, size = images.shape)
        images += np.random.randint(2, size = images.shape)
        return images

    def save_images (self, images, name='result.jpg'):
        images = images.reshape(self.n_img_x*self.n_img_y, self.img_h, self.img_w)
        imwrite(self.DIR + "/"+name, self._merge(images, [self.n_img_y, self.n_img_x]))

    def _merge(self, images, size):
        h, w = images.shape[1], images.shape[2]

        h_ = int(h * self.resize_factor)
        w_ = int(w * self.resize_factor)

        img = np.zeros((h_ * size[0], w_ * size[1]))

        for idx, image in enumerate(images):
            i = int(idx % size[1])
            j = int(idx / size[1])
            image_ = imresize(image, (w_,h_),  mode = 'reflect')
            img[j*h_:j*h_+h_, i*w_:i*w_+w_] = image_

        return img


class Plot_Manifold_Learning_Result(Plot_Reproduce_Performance):
    def __init__(self, DIR, n_img_x=20, n_img_y=20, img_w=28, img_h=28, resize_factor=1.0, z_range=2.0):
        self.DIR = DIR

        assert n_img_x > 0 and n_img_y > 0

        self.n_img_x = n_img_x
        self.n_img_y = n_img_y
        self.n_tot_imgs = n_img_x * n_img_y

        assert img_w > 0 and img_h > 0

        self.img_w = img_w
        self.img_h = img_h

        assert resize_factor > 0

        self.resize_factor = resize_factor

        assert z_range > 0
        self.z_range = z_range

        self._set_latent_vectors()

    def _set_latent_vectors(self):

        # z1 = np.linspace(-self.z_range, self.z_range, self.n_img_y)
        # z2 = np.linspace(-self.z_range, self.z_range, self.n_img_x)
        #
        # z = np.array(np.meshgrid(z1, z2))
        # z = z.reshape([-1, 2])

        # borrowed from https://github.com/fastforwardlabs/vae-tf/blob/master/plot.py
        z = np.rollaxis(np.mgrid[self.z_range:-self.z_range:self.n_img_y * 1j, self.z_range:-self.z_range:self.n_img_x * 1j], 0, 3)
        # z1 = np.rollaxis(np.mgrid[1:-1:self.n_img_y * 1j, 1:-1:self.n_img_x * 1j], 0, 3)
        # z = z1**2
        # z[z1<0] *= -1
        #
        # z = z*self.z_range

        self.z = z.reshape([-1, 2])

    # borrowed from https://github.com/ykwon0407/variational_autoencoder/blob/master/variational_bayes.ipynb
    def save_scattered_image(self, z, id, name='scattered_image.jpg'):
        N = 10
        plt.figure(figsize=(8, 6))
        plt.scatter(z[:, 0], z[:, 1], c=np.argmax(id, 1), marker='o', edgecolor='none', cmap=discrete_cmap(N, 'jet'))
        plt.colorbar(ticks=range(N))
        axes = plt.gca()
        axes.set_xlim([-self.z_range-2, self.z_range+2])
        axes.set_ylim([-self.z_range-2, self.z_range+2])
        plt.grid(True)
        plt.savefig(self.DIR + "/" + name)


# borrowed from https://gist.github.com/jakevdp/91077b0cae40f8f8244a
def discrete_cmap(N, base_cmap=None):
    """Create an N-bin discrete colormap from the specified input map"""

    # Note that if base_cmap is a string or None, you can simply do
    #    return plt.cm.get_cmap(base_cmap, N)
    # The following works for string, None, or a colormap instance:

    base = plt.cm.get_cmap(base_cmap)
    color_list = base(np.linspace(0, 1, N))
    cmap_name = base.name + str(N)
    return base.from_list(cmap_name, color_list, N)

def keras_history (history, name = 'loss'):
    plt.plot(history.history[name])
    plt.plot(history.history['val_{}'.format (name)])
    plt.title('model {}'.format (name))
    plt.ylabel(name)
    plt.xlabel('epoch')
    plt.legend(['train', 'valid'], loc='upper left')
    plt.show()

def auto_sized_figure (imgs, rows, cols):
    h, w = imgs [0].shape [:2]
    fw = int (w * cols / 16)
    fh = int (h * rows / 16)
    if fw > 16:
        fh = fh * 16 / fw
        fw = 16
    fig = plt.figure (figsize = (fw, fh))
    return fig

def gview (cols, *imgs, saveas = None, titles = None, axis = True, dpi = 100):
    if len (imgs) == 1 and isinstance (imgs [0], list):
        imgs = imgs [0]

    if len (imgs) % cols != 0:
        lacks = cols - (len (imgs) % cols)
        if lacks:
            for _ in range (lacks):
                dummy = np.zeros_like (imgs [-1], dtype = np.uint8)
                dummy [:] = 255
                imgs.append (dummy)
                titles and titles.append ('')

    rows = math.ceil (len (imgs) // cols)
    fig = auto_sized_figure (imgs, rows, cols)
    for i, img in enumerate (imgs):
        ax1 = fig.add_subplot(rows, cols, i + 1)
        if img.dtype == np.uint8:
            img = np.clip (img, 0, 255)
        else:
            img = np.clip (img, 0, 1)

        if len (img.shape) == 2:
            img = np.expand_dims (img, axis = -1)
        if img.shape [-1] == 1:
            img = np.repeat (img, 3, -1)
        if titles:
            ax1.set_title (titles [i])
        if not axis:
            ax1.axis ('off')
        ax1.imshow (img)

    if saveas:
        plt.savefig (saveas, dpi = dpi, facecolor='#eeeeee')
        plt.close()
    else:
        plt.show ()

def hview (*imgs, saveas = None, titles = None, axis = True, dpi = 100):
    if len (imgs) == 1 and isinstance (imgs [0], list):
        cols = len (imgs [0])
    else:
        cols = len (imgs)
    gview (cols, *imgs, saveas = saveas, titles = titles, axis = axis, dpi = dpi)

def vview (*imgs, saveas = None, titles = None, axis = True, dpi = 100):
    gview (1, *imgs, saveas = saveas, titles = titles, axis = axis, dpi = dpi)
