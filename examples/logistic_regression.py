import dnn
from dnn import datautil, predutil
from datasets.mnist import read_data_sets
from tqdm import tqdm
import tensorflow as tf
import numpy as np

class MNI (dnn.DNN):
    def make_place_holders (self):
        self.x = tf.compat.v1.placeholder ("float", [None, 784])
        self.y = tf.compat.v1.placeholder ("float", [None, 10])

    def make_optimizer (self):
        return self.optimizer ("adam")

    def make_cost (self):
        cost = tf.reduce_mean (input_tensor=tf.nn.softmax_cross_entropy_with_logits (
            logits = self.logit, labels = self.y
        ))
        return cost

    def performance_metric (self, r):
        return r.metric.f1.weighted

    def make_logit (self):
        with tf.compat.v1.variable_scope ("logit"):
            layer = self.dense (self.x, 512, kreg = self.l2 (0.1))
            layer = self.batch_norm_with_dropout (layer, activation = self.swish)
            layer = self.dense (layer, 256, kreg = self.l2 (0.1))
            layer = self.batch_norm (layer, activation = self.swish)
            layer = self.dense (layer, 10)
        return layer


def main (test = False):

    mnist = read_data_sets ("data", one_hot=True, test = test)
    net = MNI ()
    net.set_tensorboard_dir ("./temp/tflog")

    # test normalizinng
    net.normalize (datautil.shuffled (mnist.train.images, 10000), normalize = True, standardize = True, axis = 0)
    train_xs = net.normalize (mnist.train.images)
    print ("* Loading data...")
    print ("  - Training data mean {:.2f}, std {:.2f}, min {:.2f}, max {:.2f}".format (train_xs.mean (), train_xs.std (), train_xs.min (), train_xs.max ()))
    test_xs = net.normalize (mnist.test.images)
    print ("  - Validation data mean {:.2f}, std {:.2f}, min {:.2f}, max {:.2f}".format (test_xs.mean (), test_xs.std (), test_xs.min (), test_xs.max ()))
    print ("* Normalizing factor dimensions: {}".format (net.get_norm_factor () [0].shape))

    train_minibatches = datautil.minibatch (train_xs, mnist.train.labels, 128)
    vaild_minibatches = datautil.minibatch (train_xs, mnist.train.labels, mnist.test.num_examples)

    cherry = None
    training_epochs = test and 3 or 200
    batch_size = 128
    net.set_learning_rate (1e-2, 0.99, 100)

    for epoch in range (1, training_epochs + 1):
        print ("\n* Epoch progress: {} / {} ({:.0f}%)".format (epoch, training_epochs, epoch / training_epochs * 100))
        net.set_epoch (epoch)
        total_batch = int (mnist.train.num_examples/batch_size)
        for i in range(total_batch):
            batch_xs, batch_ys = next (train_minibatches)
            net.batch (batch_xs, batch_ys, dropout_rate = 0.3)
        resub_xs, resub_ys = next (vaild_minibatches)
        net.resub (resub_xs, resub_ys).log ()

        r = net.valid (test_xs, mnist.test.labels).log ()
        r.confusion_matrix (10)

        net.cherry.log ()
        net.cherry.confusion_matrix (10)

        if net.is_overfit:
            print ("* Overfit in progress, terminated.")
            break

if __name__ == "__main__":
    import sys

    main ("--test" in sys.argv)
